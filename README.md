# soal-shift-sisop-modul-2-ITB09-2022

Hasil Pengerjaan Soal Shift Praktikum SISTEM OPERASI 2022 kelompok ITB09<br>
Anggota Kelompok:<br>

| Nama                           | NRP          |
| -------------------------------| -------------|
| Fatih Rian Hibatul Hakim       | `5027201066` |
| Naufal Dhiya Ulhaq             | `5027201029` |
| Kevin Oktoaria                 | `5027201046` |

---

# Soal 1

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

## Soal 1a

Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

### Analisa Soal

Sebelum mengerjakan soal, saya membuat beberapa Variabel untuk memudahkan saya mengerjakan soal nomor 1. Berikut adalah variable yang dimaksud:

```c++
pthread_t tid[3]; // buat unzip
pthread_t fid[3]; // buat makefolder
pthread_t cid[3]; // buat decode
pthread_t mid[3]; // buat pindahin

// pid_t bikinfolder_music, bikinfolder_quote, extract_music, extract_quote, zeep_music, zeep_quote, unzeeplagi_music, unzeeplagi_quote;
pid_t child; // buat parent id
```

Pada soal ini mungkin sudah jelas bahwa apa yang diminta hanyalah melakukan *ekstraksi* pada file zip yang sudah di*download*. Proses *ekstraksi* tersebut dilakukan secara bersamaan menggunakan *Thread* dan juga *Fork*.

### Cara Pengerjaan

Sebelumnya, saya harus menyiapkan folder music, hasil dan quote. untuk itu saya membuat fungsi sebagai berikut:

```c++
void *makedir(void *arg)
{
    unsigned long i = 0;
    char *folderMusic[] = {"mkdir", "music", "-p", NULL};
    char *folderQuote[] = {"mkdir", "quote", "-p", NULL};
    char *folderHasil[] = {"mkdir", "Hasil", "-p", NULL};
    pthread_t id = pthread_self();
    if (pthread_equal(id, fid[0])) // thread untuk clear layar
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mkdir", folderMusic);
        }
    }
    else if (pthread_equal(id, fid[1])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mkdir", folderQuote);
        }
    }
    else if (pthread_equal(id, fid[2])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mkdir", folderHasil);
        }
    }
    return NULL;
}
```

Untuk Pengerjaannya, Tentu yang harus dilakukan adalah mendownload file Zip yang akan digunakan. Dikarenakan tidak disediakannya link untuk mendownload file yang dimaksud, maka saya mendownloadnya secara manual.

![Zip Files](https://cdn.discordapp.com/attachments/758864655828779029/965134691936124989/unknown.png)

Setelah mendownload zip yang diperlukan. Saya membuat fungsi untuk bernama `Unzeep` untuk melakukan proses *ekstraksi*. Adapun isi dari fungsi tersebut adalah sebagai berikut:

```c++
void *Unzeep(void *arg)
{
    char *unzipMusic[] = {"unzip", "music.zip", "-d", "music", NULL};
    char *unzipQuote[] = {"unzip", "quote.zip", "-d", "quote", NULL};
    unsigned long i = 0;
    pthread_t id = pthread_self();
    int status;
    if (pthread_equal(id, tid[0])) // thread untuk clear layar
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", unzipMusic);
        }
        while ((wait(&status)) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", unzipQuote);
        }
        while ((wait(&status)) > 0)
            ;
    }
    return NULL;
}
```

Pada potongan *code* yang telah ditampilkan terdapat `pthread_self`. Syntax ini berfungsi untuk membuat thread baru pada variable yang telah dibuat. lalu ada juga `pthread_equal` yang berfungsi untuk melakukan perbandingan komparasi pada kedua parameter yang telah dimasukan.

Lanjut pindah ke bagian `int main`, barulah dilakukan pengecekan sekaligus proses berjalannya thread terjadi. adapun *code* yang dimaksud adalah sebagai berikut:

```c++
    int i = 0;
    int err2;
    while (i < 2) // loop sejumlah thread
    {
        err2 = pthread_create(&(tid[i]), NULL, &Unzeep, NULL); // membuat thread
        if (err2 != 0)                                         // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err2));
        }
        else
        {
            printf("\n create thread [%d] success\n", i + 1);
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
```

Apa yang terjadi pada potongan *code* disini sebenarnya cukup mudah untuk dipahami. saya mempersiapkan *while loop* yang berisikan `i < 2` dimana 2 ini adalah proses yang akan terjalan pada *while loop* yang baru saja dibuat. ada variabel `err2` ini untuk menyimpan eror yang di *return* dari`pthread_create`. `pthread_create` sendiri berfungsi untuk menjalankan fungsi dan thread yang telah dibahas sebelumnya sebagai parameter. Setelah semua itu, dibuatlah *if condition* dimana apabila `err2` adalah bukan 0, pertanda tidak bisa dibuatnya thread seperti yang telah diajarkan pada sesilab, maka saya print seperti demikian. adapun tujuan pembuatan dari *if condition* tersebut adalah hanya untuk penanda pada masa *debuging*.

## soal 1b

Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

### Analisa Soal

pada soal ini diminta untuk melakukan proses *decode* pada hasil ekstraksi soal sebelumnya. lalu dimasukan menjadi dua pada file yang bernama `music.txt` dan `quote.txt` sesuai dengan hasil *decode* pada zip tersebut. untuk itu, saya mencari di internet dan menemukan beberapa funsi untuk men*decode*
sebagai berikut:

```c++
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; // decode

void decodeblock(unsigned char in[], char *clrstr)
{
    unsigned char out[4];
    out[0] = in[0] << 2 | in[1] >> 4;
    out[1] = in[1] << 4 | in[2] >> 2;
    out[2] = in[2] << 6 | in[3] >> 0;
    out[3] = '\0';
    strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst)
{
    int c, phase, i;
    unsigned char in[4];
    char *p;

    clrdst[0] = '\0';
    phase = 0;
    i = 0;
    while (b64src[i])
    {
        c = (int)b64src[i];
        if (c == '=')
        {
            decodeblock(in, clrdst);
            break;
        }
        p = strchr(b64, c);
        if (p)
        {
            in[phase] = p - b64;
            phase = (phase + 1) % 4;
            if (phase == 0)
            {
                decodeblock(in, clrdst);
                in[0] = in[1] = in[2] = in[3] = 0;
            }
        }
        i++;
    }
}
```

Lalu saya membuat fungsi supaya bisa menjalankan *decode* dengan thread yang telah disediakan sebagai berikut:

```c++
void *decodeFile(void *arg)
{
    FILE *ptr, *musicDec, *quoteDec;
    char str[100];
    char temp[100];

    pthread_t id = pthread_self();
    if (pthread_equal(id, cid[0]))
    {
        for (int i = 1; i < 10; i++)
        {
            sprintf(temp, "music/m%d.txt", i);
            ptr = fopen(temp, "r");
            if (ptr == NULL)
            {
                printf("Error opening file %s!", temp);
                exit(0);
            }

            while (fgets(str, 100, ptr) != NULL)
            {
                char mydst[1024] = "";
                b64_decode(str, mydst);
                musicDec = fopen("music/music.txt", "a");
                fprintf(musicDec, "%s\n", mydst);
            }
            fclose(ptr);
            fclose(musicDec);
        }
    }
    else if (pthread_equal(id, cid[1]))
    {
        for (int i = 1; i < 10; i++)
        {
            sprintf(temp, "quote/q%d.txt", i);
            ptr = fopen(temp, "r");
            if (ptr == NULL)
            {
                printf("Error opening file %s!", temp);
                exit(0);
            }

            while (fgets(str, 100, ptr) != NULL)
            {
                char mydst[1024] = "";
                b64_decode(str, mydst);
                quoteDec = fopen("quote/quote.txt", "a");
                fprintf(quoteDec, "%s\n", mydst);
            }
            fclose(ptr);
            fclose(quoteDec);
        }
    }
    return NULL;
}
```

Setelah itu, saya kembali ke `int main` dan menambah *code* berikut:

```c++
int k = 0;
    int err3;
    while (k < 2) // loop sejumlah thread
    {

        err3 = pthread_create(&(cid[k]), NULL, &decodeFile, NULL); // membuat thread
        if (err3 != 0)                                             // cek error
        {
            printf("\n can't create decode thread : [%s]", strerror(err3));
        }
        else
        {
            printf("\n decode create thread [%d] success\n", k + 1);
        }
        k++;
    }
    pthread_join(cid[0], NULL);
    pthread_join(cid[1], NULL);
```

## soal 1c

Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

### Analisa Soal

Pada bagian ini, cukup mudah. hanya perlu memindahkan file hasil decode ke folder hasil.

### Cara pengerjaan

Bagian *decode* sudah selesai, sekarang tinggal memindahkan hasil *decode* ke folder yang bernama `hasil`. untuk itu saya membuat fungsi berikut:

```c++
void *pindaheen(void *arg)
{
    int status;
    char copas1[1024];
    char copas2[1024];
    sprintf(copas1, "quote/quote.txt");
    sprintf(copas2, "music/music.txt");
    char *pindahNOWquote[] = {"mv", copas1, "Hasil", NULL};
    char *pindahNOWmusic[] = {"mv", copas2, "Hasil", NULL};
    unsigned long i = 0;
    pthread_t id = pthread_self();
    if (pthread_equal(id, mid[0])) // thread untuk clear layar
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mv", pindahNOWquote);
        }
        while ((wait(&status)) > 0)
            ;
    }
    else if (pthread_equal(id, mid[1])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mv", pindahNOWmusic);
        }
        while ((wait(&status)) > 0)
            ;
    }
    return NULL;
}
```

pada bagian `int main` dimasukan *code* berikut:

```c++
int l = 0;
    int err4;
    while (l < 2) // loop sejumlah thread
    {

        err4 = pthread_create(&(mid[l]), NULL, &pindaheen, NULL); // membuat thread
        if (err4 != 0)                                            // cel error
        {
            printf("\n move can't create decode thread : [%s]", strerror(err4));
        }
        else
        {
            printf("\n move decode create thread [%d] success\n", l + 1);
        }
        l++;
    }
    pthread_join(mid[0], NULL);
    pthread_join(mid[1], NULL);
```

## soal 1d

Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

### Analisa Soal

Pada saat ini, apa yang harus dilakukan hanyalah melakukan zip pada folder hasil. berikut fungi:

```c++
void *zeepwhoami()
{
    int status;
    chdir("/home/hakim/Documents/sisop/'modul 3'/Hasil");
    char *argv1[] = {"zip", "--password", "mihinomenesthakim", "Hasil.zip", "Hasil/music.txt", "Hasil/quote.txt", NULL};
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/zip", argv1);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
        chdir("/home/hakim/Documents/sisop/'modul 3'");
    }
}
```

pada bagian `int main` saya memasukan thread lagi sebagai berikut:

```c++
zeepwhoami();
```

## soal 1e

Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

### Analisa Soal

Dikarenakan hanya perlu mengekstrak file yang barusan di zip dan memasukan sebuah file `no.txt` yang berisikan tulisan `No`. Lalu dimasukan lagi menjadi zip

### Cara Pengerjaan

Karena banyak yang harus dilakukan. maka saya akan jabarkan dulu *code* yang saya ketik:

```c++
void *unzipfilehasilkarenagkproduktifsamaupdatenotxtsamamungkinbarenginngezipkalodahmumed()
{
    int status;
    char *unzip[] = {"unzip", "-P", "mihinomenesthakim", "Hasil.zip", NULL};
    char *pwd[] = {"pwd", NULL};
    char *bakarhasil[] = {"rm", "Hasil", "-r", NULL};
    char *bakarmusic[] = {"rm", "music", "-r", NULL};
    char *bakarquote[] = {"rm", "quote", "-r", NULL};
    char *buang[] = {"rm", "Hasil.zip", NULL};
    char *aaaa[] = {"zip", "--password", "mihinomenesthakim", "Hasil.zip", "Hasil/music.txt", "Hasil/quote.txt", "no.txt", NULL};
    child = fork();
    if (child == 0)
    {
        // execv("/usr/bin/unzip", unzip);
        execv("/usr/bin/rm", bakarhasil);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        // execv("/usr/bin/unzip", unzip);
        execv("/usr/bin/rm", bakarmusic);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        // execv("/usr/bin/unzip", unzip);
        execv("/usr/bin/rm", bakarquote);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/unzip", unzip);
        // execv("/usr/bin/pwd", pwd);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/rm", buang);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        FILE *no;
        no = fopen("no.txt", "a");
        fprintf(no, "No");
        fclose(no);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/zip", aaaa);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
}

```

pada potongan *code* tersebut ada beberapa penggunaan `rm`. Guna dari rm adalah untuk menghapus file bernama sama yang tersisa pada path yang saat ini ditempati. lalu disini ada FILE untuk membuat file bernama `no.txt` yang berisikan tulisan "no"

## output

![output](https://cdn.discordapp.com/attachments/758864655828779029/965185223463682068/unknown.png)

![output1](https://cdn.discordapp.com/attachments/758864655828779029/965185474039791638/unknown.png)

---

# Soal 2

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

## Soal 2a

Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
•	Username unique (tidak boleh ada user yang memiliki username yang sama)
•	Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

### Analisa Soal

Untuk menghubungkan dua process (client dan server), kami menggunakan socket programming. Selanjutnya, untuk melakukan action register serta login, kami membuat function-function baru sesuai dengan format yang telah diminta.

### Penyelesaian

Server
Pada bagian ini, kita akan membuat dua file coding yaitu server.c dan client.c. Untuk membuat sebuah koneksi socket antar process, maka code ditulis sebagai berikut:

Server:
```c

    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    //create socket  
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //  
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //specify address
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    //bind socket to port and address  
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    //listen to connect req
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accpeting connection
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
```

Client:
```c
    struct sockaddr_in address;
    int valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};

    //create socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    //specify address
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    
    //connect to server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
```

Setelah kedua process ter-connect, maka process client akan dilanjutkan sebagai berikut:
```c
    while (1) {
        int command;
        printf("Choose command:\n(1) register\n(2) login\n");    
        scanf("%d", &command);

        if (command == 1) {
            send(sock, "register", strlen("register"), 0);
            registuser();
        }

        if (command == 2) {
            send(sock, "login", strlen("login"), 0);
            loginuser();
        }
    }
```
dengan function-function:
```c
void registuser() {
    char idpass[100];
    printf("enter new username & pass (separated with :)\n");
    scanf("%s", idpass);
    send(sock, idpass, strlen(idpass), 0);
}

void loginuser() {
    char idpass[100];
    printf("enter new username & pass (separated with :)\n");
    scanf("%s", idpass);
    send(sock, idpass, strlen(idpass), 0);
}
```

Dengan process server memberikan respons sesuai dengan code berikut:
```c
    while (1) {
        memset(buffer, 0, 1024);
        valread = read( new_socket , buffer, 1024);
        if (strcmp(buffer,"register")==0) {
            memset(buffer, 0, 1024);
            valread = read( new_socket , buffer, 1024);
            regist(buffer);
        }
        else if (strcmp(buffer, "login")==0) {
            memset(buffer, 0, 1024);
            valread = read( new_socket , buffer, 1024);
            if (checkuser(buffer)) {
                printf("MASHOOKK PAK");
            }
        }

    }
```

dengan function-function sebagai berikut:
```c
bool checkId(char id[]){
    char temp[50];

    printf("this is function checkId");

    FILE *file = fopen("user.txt", "r");
    while (fgets(temp, 100, file))
    {
        char idtemp[20];
        sprintf(idtemp, "%s", strtok(temp,":"));
        if (strcmp(idtemp, id) == 0)
        {
            return false;
        }
    }    
    fclose(file);
    return true;    
}

void regist(char str[])
{
    printf("Registering User\n");
    char idpass[100];
    int flag;

    strcpy(idpass, str);
    printf("%s\n", idpass);

    // char id[10];
    // strcpy (id, strtok(idpass, ":"));
    // printf("%s\n", id);
    // char pass[10];
    // strcpy (pass, strtok(NULL, "\n"));
    // printf("%s\n", pass);

    // flag = checkId(id);
    // if(flag) printf("yea");

    FILE *file = fopen("user.txt", "a");
    fputs(str, file);
    fputs("\n", file);
    fclose(file);
}

bool checkuser(char str[])
{
    printf("checkuser\n");
    char idpass[100];
    strcpy(idpass, str);
    printf("%s\n", idpass);
    char *id;
    char tok[2] = ":";
    char find[100];
    FILE *file = fopen("user.txt", "r");
    while (fgets(find, 100, file))
    {
        if (strcmp(find, idpass) == 0)
        {
            return true;
        }
    }
    return false;
}
```
### Kendala

Beberapa function tidak berjalan sperti yang diinginkan :')

---

# Soal 3

### Analisa Soal

Pada soal ini, kami diminta untuk mengunzip file yang telah diberikan. Setelah itu, kami diminta untuk membuat directory untuk setiap tipe file yang ada dan lalu setiap file tersebut dimasukkan ke folder masing-masing. Lalu, folder-folder tersebut dizip dan dikirimkan.

### Cara Pengerjaan

Pada soal ini, kami menggunakan mkdir untuk membuat directory file.

```c++
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

const char *unzip[] = {"Unknown", "Hidden", "jpg", "tar.gz", "c", "png", "txt", "zip", "gif", "pdf", "jpeg", "sh", "hex", "bin", "js", "gns3project", "fbx"};

int main(void)
{
    int n = 0;
    while (n <= 17)
    {
        mkdir(unzip[n], S_IRWXU);
        n++;
    }
    return 0;
}
```

### Kendala

Masih belum bisa mengerjakan

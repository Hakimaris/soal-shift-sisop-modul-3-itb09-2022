#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

const char *unzip[] = {"Unknown", "Hidden", "jpg", "tar.gz", "c", "png", "txt", "zip", "gif", "pdf", "jpeg", "sh", "hex", "bin", "js", "gns3project", "fbx"};

int main(void)
{
    int n = 0;
    while (n <= 17)
    {
        mkdir(unzip[n], S_IRWXU);
        n++;
    }
    return 0;
}

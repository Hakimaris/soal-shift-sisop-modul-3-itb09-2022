#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

int  sock = 0;

void registuser() {
    char idpass[100];
    printf("enter new username & pass (separated with :)\n");
    scanf("%s", idpass);
    send(sock, idpass, strlen(idpass), 0);
}

void loginuser() {
    char idpass[100];
    printf("enter new username & pass (separated with :)\n");
    scanf("%s", idpass);
    send(sock, idpass, strlen(idpass), 0);
}
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};

    //create socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    //specify address
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    
    //connect to server
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while (1) {
        int command;
        printf("Choose command:\n(1) register\n(2) login\n");    
        scanf("%d", &command);

        if (command == 1) {
            send(sock, "register", strlen("register"), 0);
            registuser();
        }

        if (command == 2) {
            send(sock, "login", strlen("login"), 0);
            loginuser();
        }
    }

    return 0;
}
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

// char user[] = "Hakim";

pthread_t tid[3]; // buat unzip
pthread_t fid[3]; // buat makefolder
pthread_t cid[3]; // buat decode
pthread_t mid[3]; // buat pindahin

// pid_t bikinfolder_music, bikinfolder_quote, extract_music, extract_quote, zeep_music, zeep_quote, unzeeplagi_music, unzeeplagi_quote;
pid_t child; // buat parent id

// Decode base64 dpt dari :  https://fm4dd.com/programming/base64/base64_stringencode_c.shtm
// awal copas
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; // decode

void decodeblock(unsigned char in[], char *clrstr)
{
    unsigned char out[4];
    out[0] = in[0] << 2 | in[1] >> 4;
    out[1] = in[1] << 4 | in[2] >> 2;
    out[2] = in[2] << 6 | in[3] >> 0;
    out[3] = '\0';
    strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst)
{
    int c, phase, i;
    unsigned char in[4];
    char *p;

    clrdst[0] = '\0';
    phase = 0;
    i = 0;
    while (b64src[i])
    {
        c = (int)b64src[i];
        if (c == '=')
        {
            decodeblock(in, clrdst);
            break;
        }
        p = strchr(b64, c);
        if (p)
        {
            in[phase] = p - b64;
            phase = (phase + 1) % 4;
            if (phase == 0)
            {
                decodeblock(in, clrdst);
                in[0] = in[1] = in[2] = in[3] = 0;
            }
        }
        i++;
    }
}

void *decodeFile(void *arg)
{
    FILE *ptr, *musicDec, *quoteDec;
    char str[100];
    char temp[100];

    pthread_t id = pthread_self();
    if (pthread_equal(id, cid[0]))
    {
        for (int i = 1; i < 10; i++)
        {
            sprintf(temp, "music/m%d.txt", i);
            ptr = fopen(temp, "r");
            if (ptr == NULL)
            {
                printf("Error opening file %s!", temp);
                exit(0);
            }

            while (fgets(str, 100, ptr) != NULL)
            {
                char mydst[1024] = "";
                b64_decode(str, mydst);
                musicDec = fopen("music/music.txt", "a");
                fprintf(musicDec, "%s\n", mydst);
            }
            fclose(ptr);
            fclose(musicDec);
        }
    }
    else if (pthread_equal(id, cid[1]))
    {
        for (int i = 1; i < 10; i++)
        {
            sprintf(temp, "quote/q%d.txt", i);
            ptr = fopen(temp, "r");
            if (ptr == NULL)
            {
                printf("Error opening file %s!", temp);
                exit(0);
            }

            while (fgets(str, 100, ptr) != NULL)
            {
                char mydst[1024] = "";
                b64_decode(str, mydst);
                quoteDec = fopen("quote/quote.txt", "a");
                fprintf(quoteDec, "%s\n", mydst);
            }
            fclose(ptr);
            fclose(quoteDec);
        }
    }
    return NULL;
}
// akhir copas

void *Unzeep(void *arg)
{
    char *unzipMusic[] = {"unzip", "music.zip", "-d", "music", NULL};
    char *unzipQuote[] = {"unzip", "quote.zip", "-d", "quote", NULL};
    unsigned long i = 0;
    pthread_t id = pthread_self();
    int status;
    if (pthread_equal(id, tid[0])) // thread untuk clear layar
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", unzipMusic);
        }
        while ((wait(&status)) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/unzip", unzipQuote);
        }
        while ((wait(&status)) > 0)
            ;
    }
    return NULL;
}

void *makedir(void *arg)
{
    unsigned long i = 0;
    char *folderMusic[] = {"mkdir", "music", "-p", NULL};
    char *folderQuote[] = {"mkdir", "quote", "-p", NULL};
    char *folderHasil[] = {"mkdir", "Hasil", "-p", NULL};
    pthread_t id = pthread_self();
    if (pthread_equal(id, fid[0])) // thread untuk clear layar
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mkdir", folderMusic);
        }
    }
    else if (pthread_equal(id, fid[1])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mkdir", folderQuote);
        }
    }
    else if (pthread_equal(id, fid[2])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mkdir", folderHasil);
        }
    }
    return NULL;
}

void *pindaheen(void *arg)
{
    int status;
    char copas1[1024];
    char copas2[1024];
    sprintf(copas1, "quote/quote.txt");
    sprintf(copas2, "music/music.txt");
    char *pindahNOWquote[] = {"mv", copas1, "Hasil", NULL};
    char *pindahNOWmusic[] = {"mv", copas2, "Hasil", NULL};
    unsigned long i = 0;
    pthread_t id = pthread_self();
    if (pthread_equal(id, mid[0])) // thread untuk clear layar
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mv", pindahNOWquote);
        }
        while ((wait(&status)) > 0)
            ;
    }
    else if (pthread_equal(id, mid[1])) // thread menampilkan counter
    {
        child = fork();
        if (child == 0)
        {
            execv("/usr/bin/mv", pindahNOWmusic);
        }
        while ((wait(&status)) > 0)
            ;
    }
    return NULL;
}

void *zeepwhoami()
{
    int status;
    chdir("/home/hakim/Documents/sisop/'modul 3'/Hasil");
    char *argv1[] = {"zip", "--password", "mihinomenesthakim", "Hasil.zip", "Hasil/music.txt", "Hasil/quote.txt", NULL};
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/zip", argv1);
    }
    else
    {
        while ((wait(&status)) > 0)

            ;
        chdir("/home/hakim/Documents/sisop/'modul 3'");
    }
}

void *unzipfilehasilkarenagkproduktifsamaupdatenotxtsamamungkinbarenginngezipkalodahmumed()
{
    int status;
    char *unzip[] = {"unzip", "-P", "mihinomenesthakim", "Hasil.zip", NULL};
    char *pwd[] = {"pwd", NULL};
    char *bakarhasil[] = {"rm", "Hasil", "-r", NULL};
    char *bakarmusic[] = {"rm", "music", "-r", NULL};
    char *bakarquote[] = {"rm", "quote", "-r", NULL};
    char *buang[] = {"rm", "Hasil.zip", NULL};
    char *aaaa[] = {"zip", "--password", "mihinomenesthakim", "Hasil.zip", "Hasil/music.txt", "Hasil/quote.txt", "no.txt", NULL};
    child = fork();
    if (child == 0)
    {
        // execv("/usr/bin/unzip", unzip);
        execv("/usr/bin/rm", bakarhasil);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        // execv("/usr/bin/unzip", unzip);
        execv("/usr/bin/rm", bakarmusic);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        // execv("/usr/bin/unzip", unzip);
        execv("/usr/bin/rm", bakarquote);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/unzip", unzip);
        // execv("/usr/bin/pwd", pwd);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/rm", buang);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        FILE *no;
        no = fopen("no.txt", "a");
        fprintf(no, "No");
        fclose(no);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
    child = fork();
    if (child == 0)
    {
        execv("/usr/bin/zip", aaaa);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
    }
}

int main(void)
{
    int j = 0;
    int err1;
    while (j < 3) // loop sejumlah thread
    {
        err1 = pthread_create(&(fid[j]), NULL, &makedir, NULL); // membuat thread
        if (err1 != 0)                                          // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err1));
        }
        else
        {
            printf("\n create thread [%d] success\n", j + 1);
        }
        j++;
    }
    pthread_join(fid[0], NULL);
    pthread_join(fid[1], NULL);

    int i = 0;
    int err2;
    while (i < 2) // loop sejumlah thread
    {
        err2 = pthread_create(&(tid[i]), NULL, &Unzeep, NULL); // membuat thread
        if (err2 != 0)                                         // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err2));
        }
        else
        {
            printf("\n create thread [%d] success\n", i + 1);
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    int k = 0;
    int err3;
    while (k < 2) // loop sejumlah thread
    {

        err3 = pthread_create(&(cid[k]), NULL, &decodeFile, NULL); // membuat thread
        if (err3 != 0)                                             // cek error
        {
            printf("\n can't create decode thread : [%s]", strerror(err3));
        }
        else
        {
            printf("\n decode create thread [%d] success\n", k + 1);
        }
        k++;
    }
    pthread_join(cid[0], NULL);
    pthread_join(cid[1], NULL);

    int l = 0;
    int err4;
    while (l < 2) // loop sejumlah thread
    {

        err4 = pthread_create(&(mid[l]), NULL, &pindaheen, NULL); // membuat thread
        if (err4 != 0)                                            // cel error
        {
            printf("\n move can't create decode thread : [%s]", strerror(err4));
        }
        else
        {
            printf("\n move decode create thread [%d] success\n", l + 1);
        }
        l++;
    }
    pthread_join(mid[0], NULL);
    pthread_join(mid[1], NULL);

    zeepwhoami();
    unzipfilehasilkarenagkproduktifsamaupdatenotxtsamamungkinbarenginngezipkalodahmumed();

    exit(0);
    return 0;
}
